"""Top-level package for timeframe."""

__author__ = """Hyuksang Gwon"""
__email__ = 'gwonhyuksang@gmail.com'
__version__ = '0.1.3'

__all__ = ['write_to', 'read_from', 'append_to', 'Name']

from .timeframe import write_to, read_from, append_to, Name
