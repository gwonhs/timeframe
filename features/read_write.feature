Feature: read and write

   @fixture.tmp_location
   Scenario: read write consistency
      Given a dataframe with datetime index
      When write the dataframe using timeframe
      Then the read should be the same as the original