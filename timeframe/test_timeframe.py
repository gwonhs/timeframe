import pytest
import numpy as np
import pandas as pd

from timeframe.utils import to_df


@pytest.fixture
def _sample() -> pd.DataFrame:
    df = to_df([
        "          ,1, 2 ,3, 4",
        "2020-01-01,1,   ,2, a",
        "2020-01-02,1,   ,2, a",
        "2020-01-05,1,1.0,2, a",
    ])
    df["3"] = df["3"].astype(np.int32)
    return df


def test_sample_columns(_sample):
    assert _sample.columns.tolist() == ["1", "2", "3", "4"]


def test_sample_dtypes(_sample):
    assert _sample.dtypes.tolist() == [
        np.int64, np.float64, np.int32, np.object
    ]
