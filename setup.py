#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'tables>=3.4',
    'pandas>=0.22',
]

setup_requirements = ['pytest-runner']

test_requirements = ['pytest>=3', 'behave>=1.2.6', 'coverage>=4.5.4']

setup(
    author="Hyuksang Gwon",
    author_email='gwonhyuksang@gmail.com',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Read and write pandas.DataFrame with datetime index",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='timeframe',
    name='timeframe',
    packages=find_packages(include=['timeframe', 'timeframe.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/gwonhs/timeframe',
    version='0.1.3',
    zip_safe=False,
)
