from os import remove
from time import time

import numpy as np
import pandas as pd

from timeframe.timeframe import _write_to_f, _read_from_f


PYTABLES_F = "benchmark-pytables.hdf5"
TIMEFRAME_F = "benchmark-timeframe.hdf5"


def _index():
    return pd.date_range(start="2000-01-01", periods=20000)


def _df():
    return pd.DataFrame(np.random.rand(20000, 2000), index=_index())


def measure(func, title, n):
    def wrapper(*args, **kwargs):
        start = time()
        for _ in range(n):
            func(*args, **kwargs)
        print(f"{title} elapsed: {time() - start}")
    return wrapper


def timeframe_write(df):
    _write_to_f("blosc", 9, TIMEFRAME_F, df)


def timeframe_read(index):
    _read_from_f(TIMEFRAME_F, index[0], index[-1])


def timeframe_read_recent(index):
    _read_from_f(TIMEFRAME_F, index[-10], index[-1])


def pytables_write(df):
    with pd.HDFStore(
        PYTABLES_F,
        mode="w",
        complib="blosc",
        complevel=9
    ) as store:
        store.put("df", df, format="table")
        store.create_table_index("df", optlevel=9, kind="full")


def pytables_read(index):
    with pd.HDFStore(PYTABLES_F, "r") as store:
        store.select(
            "df",
            ("index >= pd.Timestamp('{start}') & "
             "index <= pd.Timestamp('{end}')").format(
                start=index[0],
                end=index[-1]
            )
        )


def pytables_read_recent(index):
    with pd.HDFStore(PYTABLES_F, "r") as store:
        store.select(
            "df",
            ("index >= pd.Timestamp('{start}') & "
             "index <= pd.Timestamp('{end}')").format(
                start=index[-10],
                end=index[-1]
            )
        )


if __name__ == "__main__":
    df = _df()
    index = _index()
    N_RUN = 10
    print(f"\neach test run {N_RUN} times (in seconds)\n")
    print("data: np.random.rand(30000, 2000)\n")
    measure(timeframe_write, "timeframe write", N_RUN)(df)
    measure(timeframe_read, "timeframe read whole periods", N_RUN)(index)
    measure(
        timeframe_read_recent,
        "timeframe read recent N_RUN periods", N_RUN)(index)
    print("\n")
    measure(pytables_write, "pytables write", N_RUN)(df)
    measure(pytables_read, "pytables read whole periods", N_RUN)(index)
    measure(
        pytables_read_recent,
        "pytables read recent N_RUN periods", N_RUN)(index)
    print("\n")

    remove(PYTABLES_F)
    remove(TIMEFRAME_F)
