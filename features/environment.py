import random
import string
from pathlib import Path

from behave import fixture, use_fixture


@fixture
def tmp_location(context) -> Path:
    # -- SETUP-FIXTURE PART:
    context.base_dir = Path("/tmp/timeframe-test")
    context.name = _random_string()

    yield context.name

    # -- CLEANUP-FIXTURE PART:
    path = context.base_dir / f"{context.name}.h5"

    if path.exists():
        path.unlink()


def _random_string():
    return ''.join(
        random.choices(string.ascii_uppercase + string.digits, k=30)
    )


def before_tag(context, tag):
    if tag == "fixture.tmp_location":
        use_fixture(tmp_location, context)
