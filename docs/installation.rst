.. highlight:: shell

============
Installation
============


Stable release
--------------

To install timeframe, run this command in your terminal:

.. code-block:: console

    $ pip install timeframe

This is the preferred method to install timeframe, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for timeframe can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/gwonhs/timeframe

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Gitlab repo: https://gitlab.com/gwonhs/timeframe
