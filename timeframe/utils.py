from io import StringIO

import pandas as pd


def to_df(csv) -> pd.DataFrame:
    csv = StringIO("\n".join(csv))

    df = pd.read_csv(
        csv, index_col=0, parse_dates=True,
        na_values=["   "]
    )

    df.columns = [i.strip() for i in df.columns]
    df.index.name = None

    return df
