Feature: append

   @fixture.tmp_location
   Scenario: it should append if previous timeframe exists
      Given a dumped timeframe
      When a new dataframe is appended
      Then the timeframe should be overrided and appended

   @fixture.tmp_location
   Scenario: it should write if the timeframe does not exist before
      When a new dataframe is appended
      Then the timeframe should be written