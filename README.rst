#########
timeframe
#########

.. image:: https://gitlab.com/gwonhs/timeframe/badges/master/pipeline.svg
     :target: https://gitlab.com/gwonhs/timeframe/-/commits/master

.. image:: https://gitlab.com/gwonhs/timeframe/badges/master/coverage.svg
     :target: https://gitlab.com/gwonhs/timeframe/-/commits/master



| Read and write pandas.DataFrame with datetime index
| and resolve pandas.HDFStore's limit on number of columns [`pandas#6245`_])

| np.object other than str are not supported

.. _pandas#6245: https://github.com/pandas-dev/pandas/issues/6245




Install
-------

::

        $ pip install timeframe


Unit Test
---------

::

        $ pytest


Acceptance Test
---------------

::

        $ behave


Benchmark
---------

::

        $ python benchmark.py

| each test run 10 times (in seconds)

| data: np.random.rand(30000, 2000)

+------------------------+-----------+-------------+
| Test                   | timeframe | pd.HDFStore |
+========================+===========+=============+
| write                  |     15.20 |       17.36 |
+------------------------+-----------+-------------+
| read whole periods     |      2.77 |        1.98 |
+------------------------+-----------+-------------+
| read recent 10 periods |      0.10 |        0.12 |
+------------------------+-----------+-------------+


Example
-------

.. code-block:: python

        from timeframe import write_to, append_to, read_from


        # truncate previous cache if exists
        write_to(dump_dir: pathlib.Path)(name: str, df: pd.DataFrame)

        # append_to := append( write_to(read_from(name)), new_df)
        # (not extending pytable, duplicated index will be overrided)
        append_to(dump_dir: pathlib.Path)(name: str, df: pd.DataFrame)

        read_from(dump_dir: pathlib.Path)(name: str, start: datetime, end: datetime)


License
-------
* Free software: MIT license


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
