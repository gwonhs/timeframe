from pathlib import Path
from datetime import datetime
from dataclasses import dataclass

import pandas as pd
from behave import given, when, then

from timeframe import write_to, append_to, read_from, Name
from timeframe.utils import to_df


@dataclass
class Context:
    base_dir: Path
    name: Name


@given('a dumped timeframe')
def a_dumped_timeframe(context):
    df = to_df([
        "          , 1 , 2 ",
        "2020-01-01,1.0,2.0",
        "2020-01-02,1.0,2.0",
        "2020-01-05,1.0,2.0",
    ])
    write_to(context.base_dir)(context.name, df)


@when('a new dataframe is appended')
def a_new_dataframe_is_arrived(context):
    df = to_df([
        "          , 2 , 3 ",
        "2020-01-05,   ,2.0",
        "2020-01-06,3.0,3.0",
    ])
    append_to(context.base_dir)(context.name, df)


@then('the timeframe should be overrided and appended')
def the_timeframe_should_be_overrided_and_appended(context):
    expected = to_df([
        "          , 1 , 2 , 3 ",
        "2020-01-01,1.0,2.0,   ",
        "2020-01-02,1.0,2.0,   ",
        "2020-01-05,1.0,   ,2.0",
        "2020-01-06,   ,3.0,3.0",
    ])
    start = datetime(2020, 1, 1)
    end = datetime(2020, 1, 6)
    result = read_from(context.base_dir)(context.name, start, end)
    pd.testing.assert_frame_equal(result, expected)


@then('the timeframe should be written')
def the_timeframe_should_be_written(context):
    expected = to_df([
        "          , 2 , 3 ",
        "2020-01-05,   ,2.0",
        "2020-01-06,3.0,3.0",
    ])
    start = datetime(2020, 1, 1)
    end = datetime(2020, 1, 6)
    result = read_from(context.base_dir)(context.name, start, end)
    pd.testing.assert_frame_equal(result, expected)
