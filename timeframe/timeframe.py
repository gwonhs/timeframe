"""Main module."""

from pathlib import Path
from datetime import datetime
from functools import partial
from collections import defaultdict
from contextlib import closing
from typing import Callable, Dict, List, Optional

import numpy as np
import pandas as pd
from tables import open_file, Filters


Name = str
Dtype = np.dtype
Index = int


def write_to(
    path: Path,
    complib="blosc",
    complevel=9
) -> Callable[[Name, pd.DataFrame], None]:
    path.mkdir(parents=True, exist_ok=True)
    return partial(_write, path, complib, complevel)


def _write(
    path: Path,
    complib,
    complevel,
    name: Name,
    df: pd.DataFrame
) -> None:
    _write_to_f(complib, complevel, path / f"{name}.h5", df)


def _write_to_f(
    complib,
    complevel,
    file: Path,
    df: pd.DataFrame
) -> None:
    dtype_to_col_indexes = _dtype_to_col_indexes(df.dtypes)

    filters = Filters(complib=complib, complevel=complevel)

    with closing(open_file(
        file, mode="w", title="Test file", filters=filters
    )) as h5file:

        h5file.create_array(
            h5file.root, "index", df.index.values.astype(np.float64), "index")

        h5file.create_array(
            h5file.root, "all_columns", df.columns.tolist(), "all_columns")

        data_grp = h5file.create_group(h5file.root, "data", "data group")

        h5file.create_array(
            h5file.root, "dtypes",
            [_name(i) for i in dtype_to_col_indexes.keys()],
            "dtypes"
        )

        for dtype, indexes in dtype_to_col_indexes.items():
            data = df.iloc[:, indexes]

            dtype_name = _name(dtype)

            group = h5file.create_group(
                data_grp, dtype_name, f"{dtype_name} group")

            if dtype_name == "str":
                arr = data.values
                arr = arr.astype("U")
            elif dtype_name == "datetime":
                arr = data.values.astype(np.float64)
            else:
                arr = data.values

            h5file.create_carray(
                where=group, name="data",
                obj=arr, title=f"{dtype_name} data"
            )

            h5file.create_array(
                group, "columns",
                data.columns.tolist(), f"{dtype_name} columns")


def _dtype_to_col_indexes(dtypes) -> Dict[Dtype, List[Index]]:
    result = defaultdict(list)

    for i, dtype in enumerate(dtypes):
        result[dtype].append(i)

    return result


def read_from(path: Path) -> Callable[[Name], pd.DataFrame]:
    return partial(_read, path)


def _read(
    path: Path,
    name: Name,
    start: Optional[datetime],
    end: Optional[datetime]
) -> pd.DataFrame:
    return _read_from_f(path / f"{name}.h5", start, end)


def _read_from_f(
    file: Path,
    start: Optional[datetime] = None,
    end: Optional[datetime] = None
) -> pd.DataFrame:
    with closing(open_file(file, mode="r", title="Test file")) as h5file:

        index = h5file.get_node(h5file.root, "index").read()
        index = pd.to_datetime(index)

        if start is None:
            start = 0
        else:
            start = np.searchsorted(index, start)

        if end is None:
            end = len(index)
        else:
            end = np.searchsorted(index, end, side="right")

        all_columns = h5file.get_node(h5file.root, "all_columns").read()
        dtypes = h5file.get_node(h5file.root, "dtypes").read()

        df_list = []
        for dtyp in dtypes:
            node = f"/data/{dtyp.decode()}"
            values = h5file.get_node(node, "data")[start:end]
            columns = h5file.get_node(node, "columns").read()

            if dtyp == b'str':
                values = values.astype('str')
            elif dtyp == b'datetime':
                values = np.vstack([
                    pd.to_datetime(values[:, i])
                    for i in range(values.shape[1])
                ]).T

            df = pd.DataFrame(
                index=index[start:end],
                columns=columns,
                data=values
            )
            df_list.append(df)

    df = pd.concat(df_list, axis=1)[all_columns]

    if df.columns.dtype == 'object':
        df.columns = [i.decode() for i in df.columns]

    return df


def append_to(
    path: Path,
    complib="blosc",
    complevel=9
) -> Callable[[Name, pd.DataFrame], None]:
    path.mkdir(parents=True, exist_ok=True)
    return partial(_append, path, complib, complevel)


def _append(
    path: Path,
    complib,
    complevel,
    name: Name,
    df: pd.DataFrame
) -> None:
    _append_to_f(complib, complevel, path / f"{name}.h5", df)


def _append_to_f(
    complib,
    complevel,
    file: Path,
    df: pd.DataFrame
) -> None:
    if not file.exists():
        _write_to_f(complib, complevel, file, df)
    else:
        prev = _read_from_f(file)
        new = prev.reindex(
            index=prev.index.union(df.index),
            columns=prev.columns.union(df.columns)
        )
        new.loc[df.index, df.columns] = df.values
        _write_to_f(complib, complevel, file, new)


def _name(dtype):
    if dtype.name == "object":
        return "str"
    elif dtype.name == "datetime64[ns]":
        return "datetime"
    else:
        return dtype.name
