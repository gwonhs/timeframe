from pathlib import Path
from datetime import datetime
from dataclasses import dataclass

import numpy as np
import pandas as pd
from behave import given, when, then

from timeframe import write_to, read_from, Name
from timeframe.utils import to_df


@dataclass
class Context:
    base_dir: Path
    df: pd.DataFrame
    name: Name


@given('a dataframe with datetime index')
def a_dataframe_with_datetime_index(context: Context):
    df = to_df([
        "          ,1, 2 ,3, 4,    5    ,     6     ",
        "2020-01-01,1,   ,2, a,2020-01-01,2020-01-04",
        "2020-01-02,1,   ,2, a,2020-01-02,2020-01-05",
        "2020-01-05,1,1.0,2, a,2020-01-03,2020-01-06",
    ])
    df["3"] = df["3"].astype(np.int32)
    df["5"] = pd.to_datetime(df["5"])
    df["6"] = pd.to_datetime(df["6"]).dt.to_pydatetime()
    context.df = df


@when('write the dataframe using timeframe')
def write_the_dataframe_using_timeframe(context: Context):
    write_to(context.base_dir)(context.name, context.df)


@then('the read should be the same as the original')
def the_read_should_be_the_same_as_the_original(context: Context):
    start = datetime(2020, 1, 1)
    end = datetime(2020, 1, 5)
    result = read_from(context.base_dir)(context.name, start, end)
    pd.testing.assert_frame_equal(context.df, result)
